import { AbortablePromise, ResourceLoader } from "jsdom";
import { v4 as generateUuid } from "uuid";
import { URL } from "./constants";
import { ParsedWef } from "./wef";

const BLOB_START = `blob:${URL}` as const;

class BlobLoader extends ResourceLoader {
  #files: Map<string, Buffer>;

  constructor() {
    super();
    this.#files = new Map();
  }

  addResource(contents: Buffer): string {
    const blob = BLOB_START + generateUuid();
    this.#files.set(blob, contents);
    return blob;
  }

  addResourcesFromWef({ files }: ParsedWef): Map<string, string> {
    const blobs = new Map();

    for (const [fileName, contents] of files) {
      const blob = this.addResource(contents);
      blobs.set(fileName, blob);
    }

    return blobs;
  }

  getContents(url: string) {
    const contents = this.#files.get(url);
    if (!contents) {
      throw new Error(`tried to load a non-existing blob: ${url}`);
    }

    return contents;
  }

  async domFetch(url: string) {
    const contents = this.getContents(url);

    return {
      arrayBuffer() {
        return Promise.resolve(Uint8Array.from(contents));
      },
    } as unknown as Response;
  }

  fetch(url: string) {
    return Promise.resolve(this.getContents(url)) as AbortablePromise<Buffer>;
  }
}

export default BlobLoader;
