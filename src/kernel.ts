import * as wef from "./wef";
import { JSDOM, VirtualConsole } from "jsdom";
import BlobLoader from "./blobLoader";
import { URL } from "./constants";
import { performance } from "perf_hooks";
import {
  Kernel,
  KernelParameters,
  Initramfs,
  SystemCallName,
} from "./kernelTypes";
import { Worker } from "worker_threads";
import * as path from "path";
import { once } from "events";

interface PrepareDomProps {
  blobLoader: BlobLoader;
  virtualConsole: VirtualConsole;
}

const prepareDom = ({ blobLoader, virtualConsole }: PrepareDomProps) => {
  const dom = new JSDOM(``, {
    url: URL,
    runScripts: `dangerously`,
    virtualConsole,
    resources: blobLoader,
  });

  // https://github.com/jsdom/jsdom/issues/2740
  Object.setPrototypeOf(dom.window, dom.window.Window.prototype);

  dom.window.TextDecoder = TextDecoder;
  dom.window.TextEncoder = TextEncoder;

  return dom;
};

export interface StartProps {
  kernelWef: Buffer;
  generateParameters(
    initramfs: typeof Initramfs,
    parameters: typeof KernelParameters
  ): Promise<KernelParameters>;
  frameBufferRender?(image: Uint8ClampedArray): void;
}

interface KernelExecutable {
  Kernel: typeof Kernel;
  Parameters: typeof KernelParameters;
  Initramfs: typeof Initramfs;
  memory: WebAssembly.Memory;
}

export const start = async ({
  kernelWef,
  generateParameters,
  frameBufferRender,
}: StartProps) => {
  const parsedKernelWef = wef.parse(kernelWef);

  const blobLoader = new BlobLoader();
  const kernelFileBlobs = blobLoader.addResourcesFromWef(parsedKernelWef);

  const virtualConsole = new VirtualConsole();
  virtualConsole.sendTo(console);

  const dom = prepareDom({ blobLoader, virtualConsole });

  dom.window.fetch = blobLoader.domFetch.bind(blobLoader);
  dom.window.getFileBlob = (fileName: string) => kernelFileBlobs.get(fileName);

  dom.window.processes = new Map();
  dom.window.kernel = {
    rerender: frameBufferRender
      ? () => {
          const buffer = new Uint8ClampedArray(
            memory.buffer,
            frameBuffer.buffer,
            frameBuffer.width * frameBuffer.height * 4
          );
          frameBufferRender(buffer);
        }
      : () => {},
    panic(message: string) {
      for (const process of dom.window.processes.values()) {
        process.terminate();
      }

      const error = new Error(message);
      console.error(error);

      setTimeout(() => kernelRef.panic(message, error.stack!));
    },
    now: performance.now.bind(performance),
    async startProcess(
      processId: number,
      file: Uint8Array,
      args: Buffer[],
      variables: Map<Buffer, Buffer>
    ) {
      try {
        const parsedWef = wef.parse(Buffer.from(file));
        const worker = new Worker(path.join(__dirname, `worker/index.js`), {
          workerData: {
            ...parsedWef,
            arguments: args,
            variables,
          },
        });
        dom.window.processes.set(processId, worker);

        interface SystemCall {
          id: number;
          systemCall: SystemCallName | `getProcessId`;
          arguments: unknown[];
        }

        const [firstMessage] = (await once(worker, `message`)) as [
          { id: 0; error: string | null }
        ];

        if (firstMessage.error !== null) {
          throw new Error(firstMessage.error);
        }

        worker.on(
          `message`,
          async ({ id, systemCall, arguments: args }: SystemCall) => {
            if (systemCall === `getProcessId`) {
              worker.postMessage({ id, result: processId });
              return;
            }

            if (systemCall === `exit`) {
              this.terminateProcess(processId);
              await kernelRef.clone().exitSystemCall(processId, ...args);
              return;
            }

            const method = `${systemCall}SystemCall` as const;
            try {
              const result = await kernelRef
                .clone()
                [method](processId, ...args);
              worker.postMessage({ id, result });
            } catch (error) {
              console.error(error);
            }
          }
        );
      } catch (error) {
        console.error(error);
        throw error;
      }
    },
    terminateProcess(processId: number) {
      dom.window.processes.get(processId)?.terminate();
      dom.window.processes.delete(processId);
    },
    executeCode(code: string) {
      return JSON.stringify(dom.window.Function(code)()) || ``;
    },
  };

  process.on(`unhandledRejection`, (error) => {
    console.error(error);
  });

  const loadKernel = (): Promise<KernelExecutable> =>
    new Promise((resolve) => {
      const entryScriptElement = dom.window.document.createElement(`script`);
      entryScriptElement.src = kernelFileBlobs.get(parsedKernelWef.entry)!;
      dom.window.document.head.appendChild(entryScriptElement);
      entryScriptElement.addEventListener(`load`, () => {
        dom.window.executable.then(resolve);
      });
    });

  const { Kernel, Parameters, Initramfs, memory } = await loadKernel();
  const parameters = await generateParameters(Initramfs, Parameters);
  const kernel = Kernel.new(parameters);
  const frameBuffer = kernel.getFrameBuffer();

  const kernelRef = kernel.intoRef();
  kernelRef.clone().init();

  return {
    dom,
    getKernelRef() {
      return kernelRef.clone();
    },
  };
};
