export declare enum KeyEventKind {
  Up,
  Down,
  Hold,
}

export declare class AnyForWasm {}

export declare class FrameBuffer {}

export declare class FrameBufferView {
  buffer: number;
  height: number;
  width: number;
}

export declare class Initramfs {
  static new(): Initramfs;
  from_idbfs(id: string): any;
  createDirectory(path: string): any;
  createFile(path: string, contents: Uint8Array): any;
  createExecutable(path: string, contents: Uint8Array): any;
  intoAny(): AnyForWasm;
}

export declare class Kernel {
  static new(parameters: KernelParameters): Kernel;
  getFrameBuffer(): FrameBufferView;
  intoRef(): KernelRef;
}

export type SystemCallName =
  | `awaitProcess`
  | `canonicalize`
  | `close`
  | `execute`
  | `exit`
  | `flush`
  | `readDirectory`
  | `readFileMetadata`
  | `readMetadata`
  | `removeDirectory`
  | `setCurrentWorkingDirectory`
  | `unlinkFile`
  | `unmount`
  | `keyboar`
  | `duplicate`
  | `getCurrentWorkingDirectory`
  | `createPipe`
  | `getExecutablePath`
  | `open`
  | `read`
  | `rename`
  | `seek`
  | `setPermissions`
  | `createDirectory`
  | `createHardLink`
  | `mount`
  | `setFilePermissions`
  | `write`;

type SystemCallMethodName = `${SystemCallName}SystemCall`;

export declare class KernelRefBase {
  clone(): KernelRef;
  panic(message: string, stack: string): void;
  init(): any;
  blinkInterrupt(): void;
}

type KernelRef = KernelRefBase &
  {
    [systemCall in SystemCallMethodName]: (
      ...args: unknown[]
    ) => Promise<unknown>;
  };

export declare class KernelParameters {
  static new(
    screen_width: number,
    screen_height: number,
    screen_dpi: number,
    initramfs: AnyForWasm
  ): KernelParameters;
}
