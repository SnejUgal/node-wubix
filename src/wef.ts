export interface ParsedWef {
  files: Map<string, Buffer>;
  entry: string;
}

export const parse = (wef: Buffer): ParsedWef => {
  const metadataEnd = wef.indexOf(0);
  if (metadataEnd == -1) {
    throw new Error(`Invalid executable file`);
  }

  const metadataBytes = wef.slice(0, metadataEnd);
  const metadata = JSON.parse(metadataBytes.toString(`utf8`));

  const files = new Map();

  let offset = metadataEnd + 1;
  for (const [fileName, length] of metadata.files) {
    const contents = wef.slice(offset, offset + length);
    files.set(fileName, contents);
    offset += length;
  }

  return { files, entry: metadata.entry };
};
