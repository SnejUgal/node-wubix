import * as crypto from "crypto";

class Crypto {
  getRandomValues<T extends NodeJS.ArrayBufferView>(typedArray: T): T {
    crypto.randomFillSync(typedArray);
    return typedArray;
  }
}

export default Crypto;
