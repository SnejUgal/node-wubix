import WorkerGlobalScope from "./workerGlobalScope";
import { parentPort, workerData } from "worker_threads";
import BlobLoader from "../blobLoader";

const main = async () => {
  const blobLoader = new BlobLoader();
  const files = new Map(
    [...workerData.files.entries()].map(([fileName, contents]) => [
      fileName,
      Buffer.from(contents),
    ])
  );
  const blobs = blobLoader.addResourcesFromWef({
    files,
    entry: workerData.entry,
  });
  const globalScope = new WorkerGlobalScope(blobLoader, blobs);
  globalScope.importScripts(blobs.get(workerData.entry)!);

  const sigill = (error: Error) => {
    console.error(error);
    // todo: should actually receive SIGSEGV
    globalScope.systemCalls.exit(127);
  };

  const executable = await globalScope.executable;
  if (executable && `start` in executable) {
    parentPort!.postMessage({ id: 0, error: null });

    setTimeout(async () => {
      if (`__librust_init` in executable) {
        executable.__librust_init(workerData.arguments, workerData.variables);
      }

      process.on(`unhandledRejection`, sigill);
      process.on(`uncaughtException`, sigill);

      try {
        await executable.start();
      } catch (error) {
        sigill(error);
      }
      await globalScope.systemCalls.exit(0);
    });
  } else {
    parentPort!.postMessage({ id: 0, error: `No start method` });
  }
};

main();
