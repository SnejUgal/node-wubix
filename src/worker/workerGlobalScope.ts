import { createContext, runInContext } from "vm";
import { parentPort } from "worker_threads";
import BlobLoader from "../blobLoader";
import Crypto from "./crypto";

const kernelSystemCalls = [
  `exit`,
  `open`,
  `read`,
  `write`,
  `flush`,
  `seek`,
  `readMetadata`,
  `readFileMetadata`,
  `setPermissions`,
  `setFilePermissions`,
  `close`,
  `getProcessId`,
  `readDirectory`,
  `unlinkFile`,
  `removeDirectory`,
  `canonicalize`,
  `createDirectory`,
  `createHardLink`,
  `rename`,
  `getCurrentWorkingDirectory`,
  `setCurrentWorkingDirectory`,
  `getExecutablePath`,
  `createPipe`,
  `execute`,
  `awaitProcess`,
  `mount`,
  `unmount`,
  `duplicate`,
  `kill`,
];

interface SystemCalls {
  [systemCall: string]: (...args: any[]) => Promise<unknown>;
}

interface SystemCallResult {
  id: number;
  result: unknown;
}

interface Executable {
  start(): Promise<void>;
  __librust_init(args: Buffer[], variables: Map<Buffer, Buffer>): void;
}

class WorkerGlobalScope {
  self: WorkerGlobalScope;
  WorkerGlobalScope = WorkerGlobalScope;
  systemCalls: SystemCalls;
  #blobLoader: BlobLoader;
  #fileToBlobMap: Map<string, string>;
  executable: Promise<Executable> | undefined;
  TextDecoder = TextDecoder;
  TextEncoder = TextEncoder;
  Crypto = Crypto;
  crypto = new Crypto();

  constructor(blobLoader: BlobLoader, fileToBlobMap: Map<string, string>) {
    this.#blobLoader = blobLoader;
    this.#fileToBlobMap = fileToBlobMap;
    this.self = this;
    createContext(this);

    this.systemCalls = {
      sleep(duration: number) {
        return new Promise((resolve) => setTimeout(resolve, duration));
      },
      panic: async (message: string) => {
        await this.systemCalls.write(2, message);
        await this.systemCalls.flush(2);
        await this.systemCalls.exit(1);
      },
    };

    const waitingForSystemCalls = new Map();
    parentPort!.on(`message`, (event: SystemCallResult) => {
      if (`result` in event) {
        let resolve = waitingForSystemCalls.get(event.id);
        waitingForSystemCalls.delete(event.id);
        resolve(event.result);
      }
    });

    let nextMessageId = 1;
    for (const systemCall of kernelSystemCalls) {
      this.systemCalls[systemCall] = (...args) => {
        let id = nextMessageId++;
        parentPort!.postMessage({
          id,
          systemCall,
          arguments: args,
        });
        return new Promise((resolve) => waitingForSystemCalls.set(id, resolve));
      };
    }
  }

  importScripts = (...urls: string[]) => {
    for (const url of urls) {
      const code = this.#blobLoader.getContents(url).toString(`utf-8`);
      runInContext(code, this, { filename: url });
    }
  };

  fetch = (url: string) => {
    return this.#blobLoader.domFetch(url);
  };

  getFileBlob = (name: string) => {
    return this.#fileToBlobMap.get(name);
  };
}

export default WorkerGlobalScope;
