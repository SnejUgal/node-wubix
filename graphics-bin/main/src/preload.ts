import { ipcRenderer, contextBridge } from "electron";

contextBridge.exposeInMainWorld(`main`, {
  ready() {
    ipcRenderer.send(`ready`);
  },
  onFrame(callback: (frame: Uint8ClampedArray) => void) {
    ipcRenderer.on(`frame`, (event, frame) => {
      callback(Uint8ClampedArray.from(frame));
    });
  },
});
