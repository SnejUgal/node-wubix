import { app, BrowserWindow, ipcMain, webContents } from "electron";
import * as kernel from "node-wubix/build/kernel";
import { Initramfs } from "node-wubix/build/kernelTypes";
import { promises as fs } from "fs";
import * as path from "path";

const generateInitramfs = async (initramfs: Initramfs) => {
  const base = process.argv[3];

  const createDirectory = async (directoryPath: string) => {
    initramfs = await initramfs.createDirectory(
      `/` + path.relative(base, directoryPath)
    );
    const entries = await fs.readdir(directoryPath, { withFileTypes: true });

    for (const entry of entries) {
      const entryHostPath = path.join(directoryPath, entry.name);

      if (entry.isDirectory()) {
        await createDirectory(entryHostPath);
        continue;
      }

      const stats = await fs.stat(entryHostPath);
      const creator =
        stats.mode & 0o100 ? initramfs.createExecutable : initramfs.createFile;

      const entryVirtualPath = `/` + path.relative(base, entryHostPath);
      const contents = await fs.readFile(entryHostPath);
      initramfs = await creator.call(initramfs, entryVirtualPath, contents);
    }
  };

  await createDirectory(base);
  return initramfs;
};

const boot = async () => {
  const kernelWef = await fs.readFile(process.argv[2]);
  await kernel.start({
    kernelWef,
    async generateParameters(Initramfs, Parameters) {
      const initramfs = await generateInitramfs(Initramfs.new());
      return Parameters.new(800, 600, 1, initramfs.intoAny());
    },
    frameBufferRender(frame) {
      webContents.getAllWebContents()[0].send(`frame`, Buffer.from(frame));
    },
  });
};

let window;
app.on(`ready`, () => {
  ipcMain.on(`ready`, boot);

  window = new BrowserWindow({
    width: 800,
    height: 600,
    resizable: false,
    backgroundColor: `black`,
    title: `Wubix`,
    autoHideMenuBar: true,
    webPreferences: {
      preload: path.join(__dirname, `preload.js`),
    },
  });
  window.loadFile("../../renderer/index.html");
  window.show();
});
