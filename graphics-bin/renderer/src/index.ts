interface Main {
  ready(): void;
  onFrame(callack: (frame: Uint8ClampedArray) => void): void;
}

declare interface Window {
  main: Main;
}

const canvas = document.querySelector(`canvas`)!;
const context = canvas.getContext(`2d`)!;

canvas.width = 800;
canvas.height = 600;

window.main.onFrame((buffer) => {
  const imageData = new ImageData(buffer, canvas.width, canvas.height);
  context.putImageData(imageData, 0, 0);
});

window.main.ready();
